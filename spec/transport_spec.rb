# frozen_string_literal: true

require './bike'
require './delivery_service'

describe Transport do
  subject { described_class.new(max_weight, speed, available) }

  let(:transport_other) { described_class.new(max_weight, speed, available) }
  let(:bike) { Bike.new }
  let(:car) { Car.new('000') }
  let(:max_weight) { 10 }
  let(:speed) { 20 }
  let(:available) { true }
  let(:weight) { 50 }
  let(:distance) { 100 }
  let(:wrong_weight) { 110 }
  let(:responce_wrong) { subject.find_transport(wrong_weight, distance) }

  let(:max_weight_right) { 10 }
  let(:max_weight_wrong) { 11 }
  let(:responce_wrong) do
    subject.suitable_dimensions?(max_weight_wrong, distance)
  end
  let(:car_class_name) { Car.all.first.class.name }

  let(:another_distance) { 10 }
  let(:weight) { 20 }
  let(:wrong_weight) { 300 }
  let(:other_arg) { 30 }

  describe '#location' do
    context 'when access the attribute' do
      it 'returns current location' do
        expect(bike.location).to eq('In garage')
      end
    end
  end

  describe '#location!' do
    context 'when toggle default location value' do
      it 'returns On route' do
        bike.location!

        expect(bike.location).to eq('On route')
      end
    end
  end

  describe '#location' do
    context 'when access the attribute' do
      it 'returns current location' do
        expect(bike.location).to eq('In garage')
      end
    end
  end

  describe '#delivery_cost!' do
    context 'when pass 1 as argument' do
      let(:delivery_cost_before_iteration) { car.delivery_cost }

      it 'returns plus 1 value of delivery_cost' do
        bike.delivery_cost!(1)
        expect(bike.delivery_cost - delivery_cost_before_iteration).to eq(1)
      end
    end
  end

  describe '#number_of_deliveries' do
    context 'when call the attribute after initialize' do
      it 'returns 0 default value' do
        expect(bike.number_of_deliveries).to eq(0)
      end
    end
  end

  describe '#number_of_deliveries!' do
    context 'when toggle the default value' do
      it 'increases by 1' do
        expect { car.number_of_deliveries! }.to change(car, :number_of_deliveries).by(1)
      end
    end
  end

  describe '#all' do
    context 'when call on Class' do
      let(:all_cars_count) { Car.all.size }

      it 'returns array of at least 1 object' do
        expect(all_cars_count).to be > 0
      end
    end

    context 'when new instance initialized' do
      it 'includes the new instance' do
        expect { Car.new('000') }.to change { Car.all.count }.by(1)
      end
    end
  end

  describe '#method_missing' do
    context 'when call find_by_ method' do
      let(:cars_after_wrong_request) { Car.find_by_ }

      it 'returns nil' do
        expect(cars_after_wrong_request).to be(nil)
      end
    end
  end

  describe '#speed_to_max_weight' do
    context 'with checking responce type' do
      it 'returns 0.5' do
        expect(subject.speed_to_max_weight).to eq(0.5)
      end
    end
  end

  describe '#delivery_time' do
    let(:distance_for_1_hour_trip) { 40 }

    context 'when delivery time is more than 1 hour' do
      it 'returns the time of package delivery' do
        expect(subject.delivery_time(distance_for_1_hour_trip)).to eq('2.0')
      end
    end

    context 'with distance = 100' do
      it 'returns 5.0' do
        expect(subject.delivery_time(distance)).to eq('5.0')
      end
    end

    context 'without distance argument' do
      it 'returns nil' do
        expect(subject.delivery_time).to eq(nil)
      end
    end

    context 'with String responce comparing' do
      it 'returns 5.0' do
        expect(subject.delivery_time(distance)).to eq('5.0')
      end
    end
  end

  describe '.<=>' do
    context 'with == comparison with same objects' do
      it 'returns true' do
        expect(subject).to eq(transport_other)
      end
    end

    context 'with == comparison with different objects' do
      it 'returns false' do
        expect(subject).not_to eq(bike)
      end
    end

    context 'with <=> with objects of same type' do
      it 'does not raise error' do
        expect { subject <=> bike }.not_to raise_error
      end
    end
  end

  describe '#suitable_dimensions?' do
    context 'with wrong wight' do
      let(:responce_for_wrong_wight) { subject.suitable_dimensions?(wrong_weight, distance) }

      it 'returns false' do
        expect(responce_for_wrong_wight).to be(false)
      end
    end
  end

  describe 'inheritance relation' do
    context 'with Bike class' do
      it 'is true' do
        expect(described_class).to be > Bike
      end
    end
  end
end
