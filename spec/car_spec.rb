# frozen_string_literal: true

require './car'

describe Car do
  subject { described_class.new(registr_number) }

  let(:registr_number) { '000' }

  describe 'inherited relation' do
    context 'with Transport class' do
      it 'is true' do
        expect(described_class).to be < Transport
      end
    end
  end
end
