# frozen_string_literal: true

require './bike'

describe Bike do
  subject { described_class.new(max_weight, right_speed, available) }

  let(:max_weight) { 10 }
  let(:right_speed) { 20 }
  let(:wrong_speed) { 31 }
  let(:available) { true }
  let(:weight) { 10 }
  let(:speed) { 20 }
  let(:another_argument) { 31 }

  describe 'inherited relation' do
    context 'with Transport class' do
      it 'is true' do
        expect(described_class).to be < Transport
      end
    end
  end
end
