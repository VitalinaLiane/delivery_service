# frozen_string_literal: true

require './delivery_service'

describe DeliveryService do
  subject { described_class.new }

  let(:weight) { 10 }
  let(:distance) { 30 }
  let(:wrong_weight) { 110 }
  let(:responce_wrong) { subject.find_transport(wrong_weight, distance) }

  describe '#take_transport_from_parking during .find_transport method' do
    context 'when should less 1 transports in park' do
      it 'returns true' do
        park_before = subject.park.size
        subject.find_transport(10, 30)
        park_after = subject.park.size
        took_transports = park_before - park_after

        expect(took_transports).to eq(1)
      end
    end
  end

  describe '#put_current_transport_to_parking during .confirm_delivery' do
    context 'when should plus 1 transports in park' do
      it 'returns true' do
        subject.find_transport(10, 30)
        park_before = subject.park.size
        subject.confirm_delivery
        park_after = subject.park.size

        expect(park_after - park_before).to eq(1)
      end
    end
  end

  describe 'on initialize the availability of values' do
    context 'with suitable values current class park field' do
      it 'returns not nil' do
        expect(subject.park).not_to eq(nil)
      end
    end

    context 'with usage of undefined object values' do
      it 'raises error' do
        expect { subject.a }.to raise_error(NoMethodError)
      end
    end
  end

  describe '.find_transport - arguments count' do
    context 'with less arguments' do
      it 'raises error' do
        expect { subject.find_transport }
          .to raise_error(ArgumentError)
      end
    end

    context 'with more arguments' do
      it 'raises error' do
        expect { subject.find_transport(10, 20, 30) }
          .to raise_error(ArgumentError)
      end
    end
  end

  describe '#find_transport - arguments accordance' do
    context 'with right dimensions' do
      it 'does not raise error' do
        expect { subject.find_transport(weight, distance) }
          .not_to raise_error(ArgumentError)
      end
    end

    context 'with wrong dimensions' do
      it 'returns nil' do
        expect(responce_wrong).to eq(nil)
      end
    end
  end
end
