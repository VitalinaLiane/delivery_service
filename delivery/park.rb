# frozen_string_literal: true

module Delivery
  # Class to organize transport park during delivery processes
  class Park
    def self.init
      park = []
      (1..30).each do |_i|
        park << Bike.new
      end

      (1..20).each do |_i|
        park << Car.new(registr_number: rand(1000).to_s)
      end
      park
    end
  end
end
