# frozen_string_literal: true

module Delivery
  # Class to organize messages about delivery processes
  class TransportInfo
    def self.show_free_transports(park, moment)
      start_of_info_msg =
        if moment == :after
          'After order was finished:'
        else
          'Before delivery was started:'
        end
      bikes = park.find_all { |obj| obj.class.to_s == 'Bike' }.count
      cars  = park.size - bikes
      puts "#{start_of_info_msg} #{bikes} free bikes and #{cars} free cars."
    end

    def self.show_confirm_delivery_message(current_transport)
      puts 'Delivery status: package was devivered in ' +
           current_transport.delivery_time(-1) + ' h with ' +
           current_transport.class.to_s + '.'
    end
  end
end
