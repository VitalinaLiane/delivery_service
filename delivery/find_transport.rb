# frozen_string_literal: true

module Delivery
  # Class to find available transport
  class FindTransport
    def self.check_available_transport(park, weight, distance)
      park.find do |obj|
        obj.suitable_dimensions?(weight, distance) && obj.available
      end
    end
  end
end
