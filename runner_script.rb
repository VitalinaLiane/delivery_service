# frozen_string_literal: true

require_relative 'delivery_service'

service = DeliveryService.new

def order(service, weight, distance)
  puts 'Order:'
  service.find_transport(weight, distance)
  service.confirm_delivery
  puts
end

def orders(service)
  order(service, 10, 30)
  order(service, 10, 50)
  order(service, 110, 30)
  order(service, 100, 10)
  order(service, 5, 5)
end

def comparison
  bike = Bike.new
  car = Car.new('0')

  puts 'Is bike > car ?'
  puts "Answer: #{bike > car}."
end

def all
  puts Bike.all
end

def test_find
  Bike.find_by_speed 10
end

def test_filter
  Car.filter_by_max_weight { |n| n < 101 }
end

def find_wrong_request
  puts 'Запит Bike.find_b 10:'
  Bike.find_b 10
  puts
end

def location_def
  puts Bike.all[0].location
end

orders(service)
# comparison
# all
# test_find
# test_filter
# find_wrong_request
# location_def
