## Ruby control system for delivery service. 

**Transport class has attributes:**
- location (can only be: ‘On route’, ‘In garage’)
- number_of_deliveries (number of delivered deliveries)
- delivery_cost (delivery costs by this type of transport)

‘All’ method for the Transport class returns all instances of the class 

**Bike class inherits the Transport class**
- Max Bike speed is 10 km/hr. Max package weight - 10 kg.
- Bike has a unique property - max_distance that is equal to 30 km.
  
**Car class inherits the Transport class**
- Max Car speed is 50 km/hr. Max package weight - 10 kg.
- Car has a unique property - registration number.

**DeliveryService class manages deliveries**
- DeliveryService park has several bicycles and cars.

2 methods for DeliveryService class:
- The first method finds a suitable transport for delivery according to weight and distance
- The second one - confirms delivery.

**Transport class has been extended** 
in the way that objects of this class can be compared according to delivery of 1 kg of a package.

For each attribute of Transport class: 
- the find_by_ <attr> method is implemented using metaprogramming:
- find_by method returns the first instance of a class whose attribute is equal to the parameter passed as an argument

Examples:
Car.find_by_available (true) should return the first free car

**The app is covered with tests**

