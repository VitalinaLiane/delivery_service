# frozen_string_literal: true

require_relative 'transport'

# Class to initialize Bike and check its suitable_dimensions
class Bike < Transport
  attr_reader :max_distance

  def initialize(max_weight = 10, speed = 10, available = true)
    super
    @max_distance = 30
  end

  def suitable_dimensions?(weight, distance)
    weight <= @max_weight && distance <= @max_distance
  end
end
