# frozen_string_literal: true

require_relative 'transport'

# Class to initialize Car
class Car < Transport
  attr_reader :registr_number

  def initialize(max_weight = 100, speed = 50, available = true, registr_number)
    super
    @registr_number = registr_number
  end
end
