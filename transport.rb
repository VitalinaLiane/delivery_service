# frozen_string_literal: true

# Parent class to check delivery_time, suitable_dimensions and make comparisons
class Transport
  include Comparable

  @@all = []
  attr_accessor :max_weight, :speed, :available, :location, :number_of_deliveries, :delivery_cost

  def initialize(max_weight, speed, available, *)
    @@all << self
    @max_weight = max_weight
    @speed      = speed
    @available  = available
    @location = 'In garage'
    @number_of_deliveries = 0
    @delivery_cost = 0
  end

  class << self
    def all
      @@all.find_all { |obj| obj.instance_of? self }
    end

    def method_missing(m, *args, &block)
      method_start_find = m[0..7]
      method_start_filter = m[0..9]

      return puts 'Wrong request format.' if m.length < 8

      @result =
        if method_start_find.eql? 'find_by_'
          method_name = m[8..-1].to_sym
          @@all.find do |obj|
            obj.methods.include?(method_name) && obj.__send__(method_name).eql?(args[0])
          end
        elsif method_start_filter.eql? 'filter_by_'
          method_name = m[10..-1].to_sym

          @hash = {}
          # put element attribute value object_id as key, element as value of pair
          all.each do |el|
            @hash[el.__send__(method_name).to_s.object_id] = el
          end

          res = []
          @hash.each do |k, v|
            cur_obj = []
            # make value of element attribute available again by object_id
            # and put it into array to make available for select method
            cur_obj << ObjectSpace._id2ref(k).to_i
            # put object in resulted array if value of element
            # attribute compliants to block
            res << v if cur_obj.select(&block).any?
          end

          res
        end

      puts 'Can not find transport by argument(s).' unless @result
      puts @result
    end
  end

  def location!
    locations = ['In garage', 'On route']
    locations.delete(@location)
    @location = locations[0]
  end

  def number_of_deliveries!
    @number_of_deliveries += 1
  end

  def delivery_cost!(sum)
    @delivery_cost += sum
  end

  def respond_to_missing?(*_args)
    true
  end

  def delivery_time(distance = -1)
    @delivery_time = time_description(distance) unless distance == -1
    @delivery_time
  end

  def suitable_dimensions?(weight, _distance)
    weight <= @max_weight
  end

  def speed_to_max_weight
    @max_weight / @speed.to_f
  end

  def <=>(other)
    if speed_to_max_weight < other.speed_to_max_weight
      -1
    elsif speed_to_max_weight > other.speed_to_max_weight
      1
    else
      0
    end
  end

  private

  def time_description(distance)
    delivery_time = (distance.to_f / @speed).to_s
    if delivery_time.to_i.zero?
      'less than 1'
    else
      delivery_time
    end
  end
end
