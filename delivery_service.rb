# frozen_string_literal: true

require_relative 'bike'
require_relative 'car'
require './delivery/find_transport'
require './delivery/transport_info'
require './delivery/park'

# Class to find transport, confirm_delivery and orginize transport park
class DeliveryService
  def initialize
    @park = Delivery::Park.init
  end

  attr_reader :park

  def find_transport(weight, distance)
    @current_transport = Delivery::FindTransport
                         .check_available_transport(@park, weight, distance)
    if @current_transport
      @current_transport.delivery_time(distance)
      puts "Found and took free #{@current_transport.class} from parking."
      take_transport_from_parking
    else
      puts 'Sorry. No available transport for these dimensions.'
    end
    Delivery::TransportInfo.show_free_transports(@park, :before)
  end

  def confirm_delivery
    if @current_transport
      Delivery::TransportInfo
        .show_confirm_delivery_message(@current_transport)
      put_current_transport_to_parking
    else
      puts 'Delivery status: there was not available transport for delivery.'
    end
    Delivery::TransportInfo.show_free_transports(@park, :after)
  end

  private

  def take_transport_from_parking
    index = @park.index @current_transport
    @park.delete_at index
    @current_transport.location!
  end

  def put_current_transport_to_parking
    @current_transport.location!
    @current_transport.number_of_deliveries!
    @park << @current_transport
    @current_transport = nil
  end
end
